# -*- coding: utf-8 -*-
from openerp import  api, fields, models




"""
class MailThread(models.AbstractModel):
    _inherit = 'mail.thread'
"""
class res_partner(models.Model):
    _inherit = 'res.partner'

    ccm_message_ids = fields.One2many('mail.message', 'ccm_partner_id', 
            string='CCM Messages', auto_join=True
            )