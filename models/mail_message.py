# -*- coding: utf-8 -*-
from openerp import  api, fields, models




class mail_message(models.Model):
    _inherit = 'mail.message'

    ccm_partner_id=fields.Many2one('res.partner','Partner',compute="compute_ccm_partner",store=True)
    color = fields.Char(string='Color')

    @api.depends('model','res_id')
    @api.one
    def compute_ccm_partner(self):
      if not self.model or not self.res_id :
            return False
      if self.model == 'res.partner':
        self.ccm_partner_id = self.res_id
        return True

      partner_model = self.env['ccm.message.partner_model'].sudo().search([('model.model','=',self.model)],limit=1)
      if len(partner_model):
        partner_id  = self.env[self.model].search_read([('id','=',self.res_id)],[partner_model.partner_field.name])
        if len(partner_id) and partner_id[0][partner_model.partner_field.name]:
          self.ccm_partner_id = partner_id[0][partner_model.partner_field.name][0]
          self.color = partner_model.color

