# -*- coding: utf-8 -*-
from openerp import api, fields, models
import logging

_logger = logging.getLogger(__name__)

class ccm_partner_ccm(models.Model):
   
    _name = 'ccm.message.partner_model'
    _description = 'Models partner fields'

    model = fields.Many2one('ir.model',string="model", required=True)
    partner_field = fields.Many2one('ir.model.fields',string="fields", required=True,
   							 	domain="[('model_id','=',model),('relation','=','res.partner')]")
    color = fields.Char(string='Color')

    @api.one
    def update_partner(self):
    	message_ids = self.env['mail.message'].search([('model','=',self.model.model)])
    	for message in message_ids:

            partner_id  = self.env[self.model.model].search_read([('id','=',message.res_id)],[self.partner_field.name])
            if len(partner_id) and partner_id[0][self.partner_field.name]:
               message.write({'ccm_partner_id':partner_id[0][self.partner_field.name][0],'color':self.color})


